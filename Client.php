<?php

$context = new Context(new StrategyA1(), new StrategyB2());
$context->doA();
$context->doB();

$context = new Context(new StrategyA2(), new StrategyB1());
$context->doA();
$context->doB();
