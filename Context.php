<?php

class Context
{
    /**
     * @var StrategyA
     */
    protected $strategyA;

    /**
     * @var StrategyB
     */
    protected $strategyB;

    /**
     * @param StrategyA $strategyA
     * @param StrategyB $strategyB
     */
    function __construct(StrategyA $strategyA, StrategyB $strategyB)
    {
        $this->strategyA = $strategyA;
        $this->strategyB = $strategyB;
    }

    public function doA()
    {
        $this->strategyA->doA();
    }

    public function doB()
    {
        $this->strategyB->doB();
    }
} 
